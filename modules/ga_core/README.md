# G-A Launchpad Drush
`php drush cex-to-launchpad`

`php drush ctl`

This command will perform config export from **config/default/sync/** to **docroot/profiles/ga_launchpad/config/install/**

`php drush cim-from-launchpad`

`php drush cfl`

This command that will perform config import from **docroot/profiles/ga_launchpad/config/install/** to **config/default/sync/**
