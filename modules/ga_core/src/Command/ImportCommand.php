<?php

namespace Drupal\ga_core\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
// @codingStandardsIgnoreStart
use Drupal\Console\Annotations\DrupalCommand;
// @codingStandardsIgnoreEND

/**
 * Class ImportCommand.
 *
 * @package Drupal\ga_core
 *
 * @DrupalCommand (
 *     extension="ga_core",
 *     extensionType="module"
 * )
 */
class ImportCommand extends CommandBase {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('cim-from-launchpad')
      ->setAliases(['cfl'])
      ->setDescription('Import only docroot/profiles/ga_launchpad/config/install/ to config/default/sync/')
      ->addOption(NULL, InputOption::VALUE_OPTIONAL);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->setupIo($input, $output);
    try {
      // Make the magic happen.
      $this->cliService->ImportFromLaunchpad($this->getIo(), [$this, 't'], $input->getOption('yes'));
    }
    catch (\Exception $e) {
      $this->getIo()->error($e->getMessage());
    }
  }

}
