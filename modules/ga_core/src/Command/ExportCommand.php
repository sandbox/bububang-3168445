<?php

namespace Drupal\ga_core\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
// @codingStandardsIgnoreStart
use Drupal\Console\Annotations\DrupalCommand;
// @codingStandardsIgnoreEND

/**
 * Class ExportCommand.
 *
 * @package Drupal\ga_core
 *
 * @DrupalCommand (
 *     extension="ga_core",
 *     extensionType="module"
 * )
 */
class ExportCommand extends CommandBase {

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this
      ->setName('cex-to-launchpad')
      ->setAliases(['ctl'])
      ->setDescription('Export only configuration from config/default/sync/ to docroot/profiles/ga_launchpad/config/install/')
      ->addOption('split', NULL, InputOption::VALUE_OPTIONAL);
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->setupIo($input, $output);
    try {
      // Make the magic happen.
      $this->cliService->ExportToLaunchpad($input->getOption('split'), $this->getIo(), [$this, 't'], $input->getOption('yes'));
    }
    catch (\Exception $e) {
      $this->getIo()->error($e->getMessage());
    }
  }

}
