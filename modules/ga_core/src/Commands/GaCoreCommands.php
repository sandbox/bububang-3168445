<?php

namespace Drupal\ga_core\Commands;

use Drupal\ga_core\GaCoreCliService;
use Drush\Commands\DrushCommands;

/**
 * Class GaCoreCommands.
 *
 * This is the Drush 9 command.
 *
 * @package Drupal\ga_core\Commands
 */
class GaCoreCommands extends DrushCommands {

  /**
   * The interoperability cli service.
   *
   * @var \Drupal\ga_core\GaCoreCliService
   */
  protected $cliService;

  /**
   * GaCoreCommands constructor.
   *
   * @param \Drupal\ga_core\GaCoreCliService $cliService
   *   The CLI service which allows interoperability.
   */
  public function __construct(GaCoreCliService $cliService) {
    $this->cliService = $cliService;
  }

  /**
   * Export only configuration from config/default/sync/ to docroot/profiles/ga_launchpad/config/install/.
   *
   * @command cex-to-launchpad
   *
   * @usage drush cex-to-launchpad
   *   Export configuration.
   *
   * @aliases ctl
   */
  public function ExportToLaunchpad() {
    $this->cliService->ExportToLaunchpad($this->io(), 'dt');
  }

  /**
   * Import only config from docroot/profiles/ga_launchpad/config/install/ to config/default/sync/.
   *
   * @command cim-from-launchpad
   *
   * @usage drush cim-from-launchpad
   *   Import configuration.
   *
   * @aliases cfl
   */
  public function ImportFromLaunchpad() {
    $this->cliService->ImportFromLaunchpad($this->io(), 'dt');
  }

}
