<?php

namespace Drupal\ga_core;

use Drupal\config_filter\ConfigFilterManagerInterface;
use Drupal\config_filter\ConfigFilterStorageFactory;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\StringTranslation\TranslationInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class GaCoreCliService.
 *
 * @package Drupal\ga_core
 *
 * @internal This service is not an api and may change at any time.
 */
class GaCoreCliService {

  /**
   * The return value indicating no changes were imported.
   */
  const NO_CHANGES = 'no_changes';

  /**
   * The return value indicating that the import is already in progress.
   */
  const ALREADY_IMPORTING = 'already_importing';

  /**
   * The return value indicating that the process is complete.
   */
  const COMPLETE = 'complete';

  /**
   * The filter manager.
   *
   * @var \Drupal\config_filter\ConfigFilterManagerInterface
   */
  protected $configFilterManager;

  /**
   * The config filter storage factory.
   *
   * @var \Drupal\config_filter\ConfigFilterStorageFactory
   */
  protected $storageFactory;

  /**
   * Drupal\Core\Config\ConfigManager definition.
   *
   * @var \Drupal\Core\Config\ConfigManager
   */
  protected $configManager;

  /**
   * Active Config Storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $activeStorage;

  /**
   * Sync Config Storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $syncStorage;

  /**
   * Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher definition.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Drupal\Core\ProxyClass\Lock\DatabaseLockBackend definition.
   *
   * @var \Drupal\Core\ProxyClass\Lock\DatabaseLockBackend
   */
  protected $lock;

  /**
   * Drupal\Core\Config\TypedConfigManager definition.
   *
   * @var \Drupal\Core\Config\TypedConfigManager
   */
  protected $configTyped;

  /**
   * Drupal\Core\Extension\ModuleHandler definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Drupal\Core\ProxyClass\Extension\ModuleInstaller definition.
   *
   * @var \Drupal\Core\ProxyClass\Extension\ModuleInstaller
   */
  protected $moduleInstaller;

  /**
   * Drupal\Core\Extension\ThemeHandler definition.
   *
   * @var \Drupal\Core\Extension\ThemeHandler
   */
  protected $themeHandler;

  /**
   * Drupal\Core\StringTranslation\TranslationManager definition.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $stringTranslation;

  /**
   * List of messages.
   *
   * @var array
   */
  protected $errors;

  protected $configFactory;
  /**
   * Constructor.
   */
  public function __construct(
    ConfigFilterManagerInterface $config_filter_manager,
    ConfigFilterStorageFactory $storageFactory,
    ConfigManagerInterface $config_manager,
    StorageInterface $active_storage,
    StorageInterface $sync_storage,
    EventDispatcherInterface $event_dispatcher,
    LockBackendInterface $lock,
    TypedConfigManagerInterface $config_typed,
    ModuleHandlerInterface $module_handler,
    ModuleInstallerInterface $module_installer,
    ThemeHandlerInterface $theme_handler,
    TranslationInterface $string_translation,
    ConfigFactory $config_factory
  ) {
    $this->configFilterManager = $config_filter_manager;
    $this->storageFactory = $storageFactory;
    $this->configManager = $config_manager;
    $this->activeStorage = $active_storage;
    $this->syncStorage = $sync_storage;
    $this->eventDispatcher = $event_dispatcher;
    $this->lock = $lock;
    $this->configTyped = $config_typed;
    $this->moduleHandler = $module_handler;
    $this->moduleInstaller = $module_installer;
    $this->themeHandler = $theme_handler;
    $this->stringTranslation = $string_translation;
    $this->configFactory = $config_factory;
    $this->errors = [];
  }

  /**
   * Import only docroot/profiles/ga_launchpad/config/install/ to config/default/sync/.
   *
   * @param $io
   * @param callable $t
   * @param bool $confirmed
   * @throws
   */
  public function ImportFromLaunchpad($io, callable $t, $confirmed = FALSE) {
    $name = "docroot/profiles/ga_launchpad/config/install/ to config/default/sync/";
    $io->title($t('GA Config Import to Launchpad'));
    $sync = Settings::get('config_sync_directory');
    $fs = new Filesystem();
    if(!$fs->exists(DRUPAL_ROOT."/profiles/ga_launchpad/config/install")) {
      $confirm = 1;
    }else{
      if($io->confirm($t('Directory exists, are you confirm <fg=red>%name</> is the right directory?',['%name'=>$name]))){
        $confirm =1;
      }
    }

    if(!empty($confirm)){
      $this->copyr(DRUPAL_ROOT."/profiles/ga_launchpad/config/install",DRUPAL_ROOT."/".$sync);
      $io->success($t("Successfully import file from ".$name));
    }
  }

  /**
   * Export only configuration from config/default/sync/ to docroot/profiles/ga_launchpad/config/install/.
   *
   * @param $io
   * @param callable $t
   * @param bool $confirmed
   * @throws
   */
  public function ExportToLaunchpad($io, callable $t, $confirmed = FALSE) {
    $name = "config/default/sync/ to docroot/profiles/ga_launchpad/config/install/";
    $io->title($t('GA Config export to Launchpad'));
    $sync = Settings::get('config_sync_directory');
    $fs = new Filesystem();
      if(!$fs->exists(DRUPAL_ROOT."/".$sync)) {
        $confirm = 1;
      }else{
        if($io->confirm($t('Directory exists, are you confirm <fg=red>%name</> is the right directory?',['%name'=>$name]))){
          $confirm =1;
        }
      }

      if(!empty($confirm)){
        $this->copyr(DRUPAL_ROOT."/".$sync,DRUPAL_ROOT."/profiles/ga_launchpad/config/install/");
        $io->success($t("Successfully export file from ".$name));
    }
  }

  /**
   * recursive function to copy all subdirectories and contents.
   *
   * @param string $source
   *   The source path.
   *
   * @param string $dest
   *   The dest path.
   */
  protected function copyr($source, $dest) {
    if(is_dir($source)) {
      $dir_handle=opendir($source);
      $sourcefolder = basename($source);
      mkdir($dest."/".$sourcefolder);
      while($file=readdir($dir_handle)){
        if($file!="." && $file!=".."){
          if(is_dir($source."/".$file)){
            self::copyr($source."/".$file, $dest."/".$sourcefolder);
          } else {
            copy($source."/".$file, $dest."/".$file);
          }
        }
      }
      closedir($dir_handle);
    } else {
      // can also handle simple copy commands
      copy($source, $dest);
    }
  }
}
