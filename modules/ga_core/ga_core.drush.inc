<?php

/**
 * @file
 * Drush integration for the config module.
 */

use Drush\Log\LogLevel;

/**
 * Implements hook_drush_command().
 */
function ga_core_drush_command() {
  $items['cex-to-launchpad'] = [
    'description' => 'config export from config/default/sync/ to docroot/profiles/ga_launchpad/config/install/.',
    'core' => ['8+'],
    'aliases' => ['ctl'],
    'arguments' => [
      'split' => 'The split configuration to export, if none is given do a normal export.',
    ],
    'options' => [],
    'examples' => [
      'drush cex-to-launchpad' => 'Export development configuration.',
    ],
  ];

  $items['cim-from-launchpad'] = [
    'description' => 'config import from docroot/profiles/ga_launchpad/config/install/ to config/default/sync/.',
    'core' => ['8+'],
    'aliases' => ['cfl'],
    'arguments' => [
      'split' => 'The split configuration to export, if none is given do a normal import.',
    ],
    'options' => [],
    'examples' => [
      'drush cim-from-launchpad' => 'Import configuration as drush cim does.',
    ],
  ];

  return $items;
}

/**
 * Command callback: Export only configuration from config/default/sync/ to docroot/profiles/ga_launchpad/config/install/.
 */
function drush_cex_to_launchpad($split = NULL) {
  try {
    // Make the magic happen.
    \Drupal::service('ga_generator.cli')->ExportToLaunchpad($split, new ConfigSplitDrush8Io(), 'dt');
  }
  catch (Exception $e) {
    return drush_set_error('DRUSH_CONFIG_ERROR', $e->getMessage());
  }
}

/**
 * Command callback. Import only config from docroot/profiles/ga_launchpad/config/install/ to config/default/sync/.
 */
function drush_cim_from_launchpad() {
  try {
    // Make the magic happen.
    \Drupal::service('ga_generator.cli')->ImportFromLaunchpad(new ConfigSplitDrush8Io(), 'dt');
  }
  catch (Exception $e) {
    return drush_set_error('DRUSH_CONFIG_ERROR', $e->getMessage());
  }
}

// @codingStandardsIgnoreStart
/**
 * Class ConfigSplitDrush8Io.
 *
 * This is a stand in for \Symfony\Component\Console\Style\StyleInterface with
 * drush 8 so that we don't need to depend on symfony components.
 */
class ConfigSplitDrush8Io {

  public function confirm($text) {
    return drush_confirm($text);
  }

  public function success($text) {
    drush_log($text, LogLevel::SUCCESS);
  }

  public function error($text) {
    drush_log($text, LogLevel::ERROR);
  }

  public function text($text) {
    drush_log($text, LogLevel::NOTICE);
  }
}
// @codingStandardsIgnoreEnd
